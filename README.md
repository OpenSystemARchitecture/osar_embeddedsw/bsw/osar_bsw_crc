# OSAR_BSW_Crc

## General
The Crc module implements different Algorithms to perform a cyclic redundancy check.
## Abbreviations:
OSAR == Open System ARchitecture
Crc == Cyclic Redundancy Check

## Useful Links:
- [Overall OSAR-Artifactory](http://riddiks.ddns.net:8082/artifactory/webapp/#/artifacts/browse/tree/General/prj-open-system-architecture)
- [OSAR - Crc releases](http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_EmbeddedSW/BSW/OSAR_Crc/)
- [OSAR - Documents](http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Documents/)
- [OSAR - Hyperspace](http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Hyperspace/)
  - Main-Tool to setup an OSAR Embedded Software Project.
- [OSAR - Tools](http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Tools/)
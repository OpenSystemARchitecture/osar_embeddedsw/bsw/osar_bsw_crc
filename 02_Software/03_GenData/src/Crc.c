/*****************************************************************************************************************************
 * @file        Crc.c                                                                                                        *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        21.03.2019 08:16:53                                                                                          *
 * @brief       Generated Rte Module Interface file: Crc.c                                                                   *
 * @version     1.0.0                                                                                                        *
 * @generator   OSAR Rte Module Generator v.0.1.7.10                                                                         *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.6.2                                                                           *
*****************************************************************************************************************************/

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup Crc 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Types.h"
#include "Rte_Crc.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of USER include and definition area           << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Det.h"
#include "Crc.h"
#include "Crc32_0x04C11DB7.h"


/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           End of USER include and definition area            << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map functions into code memory ---------------------------------------------*/
#define Crc_START_SEC_CODE
#include "Crc_MemMap.h"
/**
 * @brief           Rte generated module initialization function.
 * @param[in]       None
 * @retval          None
 * @note            Trigger: System Init
 * @uuid            348df43390ce4e839d12a5efcf202c08
 */
VAR(void) Crc_Init( VAR(void) )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  #if (CRC_USE_SOFT_CALC_CRC32_0X04C11DB7 == STD_ON)
  Crc32_0x04C11DB7_Init();
  #endif
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

/**
 * @brief           Rte generated module cyclic function.
 * @param[in]       None
 * @retval          None
 * @note            Trigger: Cyclic 100ms
 * @uuid            fc6f9694d719423c86aafecc986cc904
 */
VAR(void) Crc_Mainfunction( VAR(void) )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  #if (CRC_USE_SOFT_CALC_CRC32_0X04C11DB7 == STD_ON)
  #endif
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

#define Crc_STOP_SEC_CODE
#include "Crc_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of USER function area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#define Crc_START_SEC_CODE
#include "Crc_MemMap.h"

/**
 * @brief           Module global memory initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called during system startup. And before calling the init and mainfunction.
 */
void Crc_InitMemory( void )
{
#if (CRC_USE_SOFT_CALC_CRC32_0X04C11DB7 == STD_ON)
  Crc32_0x04C11DB7_InitMemory();
#endif
}

#define Crc_STOP_SEC_CODE
#include "Crc_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of USER function area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */


/*****************************************************************************************************************************
 * @file        Crc32_0x04C11DB7.c                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        07.04.2018 11:40:18                                                                                          *
 * @brief       Implementation of functionalities from the "Crc32_0x04C11DB7" module.                                        *
 *                                                                                                                           *
 * @details     This Crc module implements the specifc CRC-32 calculation algorithm with the polynom 0x4C11DB7.              *
 *              This polynom is the standardized Ethernet polynom.                                                           *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.0.1.2.15                                                                          *
*****************************************************************************************************************************/
#if (CRC_USE_SOFT_CALC_CRC32_0X04C11DB7 == STD_ON)

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup Crc
 * @{
 */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Crc32_0x04C11DB7.h"
#include "Det.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define Crc_START_SEC_CONST
#include "Crc_MemMap.h"
#define Crc_STOP_SEC_CONST
#include "Crc_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define Crc_START_SEC_NOINIT_VAR
#include "Crc_MemMap.h"
#define Crc_STOP_SEC_NOINIT_VAR
#include "Crc_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define Crc_START_SEC_INIT_VAR
#include "Crc_MemMap.h"
#define Crc_STOP_SEC_INIT_VAR
#include "Crc_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define Crc_START_SEC_ZERO_INIT_VAR
#include "Crc_MemMap.h"
#define Crc_STOP_SEC_ZERO_INIT_VAR
#include "Crc_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define Crc_START_SEC_CONST
#include "Crc_MemMap.h"
#define Crc_STOP_SEC_CONST
#include "Crc_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define Crc_START_SEC_NOINIT_VAR
#include "Crc_MemMap.h"
#define Crc_STOP_SEC_NOINIT_VAR
#include "Crc_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define Crc_START_SEC_INIT_VAR
#include "Crc_MemMap.h"
#define Crc_STOP_SEC_INIT_VAR
#include "Crc_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define Crc_START_SEC_ZERO_INIT_VAR
#include "Crc_MemMap.h"
#define Crc_STOP_SEC_ZERO_INIT_VAR
#include "Crc_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map function into code memory ---------------------------------------------*/
#define Crc32_0x04C11DB7_START_SEC_CODE
#include "Crc_MemMap.h"
#define Crc32_0x04C11DB7_STOP_SEC_CODE
#include "Crc_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------------------------  ------------------------------------------------------------*/
#define Crc32_0x04C11DB7_START_SEC_CODE
#include "Crc_MemMap.h"
/**
 * @brief           Module global memory initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called during system startup. And before calling the init and mainfuntion.
 */
void Crc32_0x04C11DB7_InitMemory( void )
{

}

/**
 * @brief           Module global initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called before using the module and after the memory initialization.
 */
void Crc32_0x04C11DB7_Init( void )
{

}

/* ######################################################################################################################## */
/* ####################################### API Interfaces of Crc32_0x04C11DB7 Algorithm ################################### */
/* ######################################################################################################################## */
/**
* @brief           API to compute a CRC 32 Checksum using soft calculation with Polynom 0x04C11DB7.
*                  This function implements the standard Ethernat Polynom within IEEE802.3
* @param[in]       uint8* pointer to data where the crc shall be calculated
* @param[in]       uint16 length of data where the crc shall be calculated
* @param[out]      uint32* pointer to write the crc32 result data.
* @retval          Crc_ReturnType
*                  > CRC_E_OK
*                  > CRC_E_NULL_POINTER
*                  > CRC_E_INVALID_DATA_LENGTH
* @details         This function use the cpu to calculate the crc ond not an look up table.
*/
Crc_ReturnType Crc32_0x04C11DB7_Compute(uint8* pData, uint16 lengthOfData, uint32 *crcResult)
{
  sint16 j, idx;
  uint32 byte, crc, mask;

  idx = 0;
  crc = 0xFFFFFFFF;

  /* Check input parameters */
  if ( (NULL_PTR == pData) || (NULL_PTR == crcResult) )
  {
#if(CRC_MODULE_USE_DET == STD_ON)
    Det_ReportError(CRC_DET_MODULE_ID, CRC_E_NULL_POINTER);
#endif
    return CRC_E_NULL_POINTER;
  }

  if (NULL == lengthOfData)
  {
#if(CRC_MODULE_USE_DET == STD_ON)
    Det_ReportError(CRC_DET_MODULE_ID, CRC_E_INVALID_DATA_LENGTH);
#endif
    return CRC_E_INVALID_DATA_LENGTH;
  }

  /* Calcaulte CRC */
  for (idx = 0; idx < lengthOfData; idx++)
  {
    byte = pData[idx];
    crc = crc ^ byte;

    for (j = 7; j >= 0; j--)    // Do eight times.
    {
      mask = -(sint32)(crc & 1);
      crc = (crc >> 1) ^ (0xEDB88320 & mask);
    }
  }

  /* Set output data */
  *crcResult = ~crc;

  return CRC_E_OK;
}

#define Crc32_0x04C11DB7_STOP_SEC_CODE
#include "Crc_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

#endif

/**
 * @}
 */
/**
 * @}
 */

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.XML;
using OsarResources.Generic;

namespace CrcXsd
{
  public class CrcXml
  {
    public XmlFileVersion xmlFileVersion;
    public UInt16 detModuleID;
    public SystemState detModuleUsage;
    public SystemState crcUseSoftCalcCrc32_0x04C11DB7;
  }
}

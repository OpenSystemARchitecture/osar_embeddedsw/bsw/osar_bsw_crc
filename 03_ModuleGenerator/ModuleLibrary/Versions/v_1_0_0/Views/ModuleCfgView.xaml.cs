﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ModuleLibrary.Versions.v_1_0_0.Views
{
  /// <summary>
  /// Interaction logic for the ModuleCfgView.xaml
  /// </summary>
  public partial class ModuleCfgView : UserControl
  {
    ViewModels.Module_ViewModel newCfg;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile">Absolute Path to configuration file</param>
    /// <param name="absPathToModuleBaseFolder">Absolute Path to module base folder</param>
    public ModuleCfgView(string pathToCfgFile, string absPathToModuleBaseFolder)
    {
      newCfg = new ViewModels.Module_ViewModel(pathToCfgFile, absPathToModuleBaseFolder);

      InitializeComponent();

      /* Set Data Context where data is binded to*/
      this.DataContext = newCfg;
    }
  }
}

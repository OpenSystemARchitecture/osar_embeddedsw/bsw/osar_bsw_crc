﻿/*****************************************************************************************************************************
 * @file        ModuleGenerator.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Module Generator Class                                                                 *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_1_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.Generator.Resources;
using System.Reflection;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using OsarSourceFileLib.DocumentationObjects.Doxygen;
using OsarSourceFileLib.CFile;
using OsarSourceFileLib.CFile.CFileObjects;
using OsarSourceFileLib.DocumentationObjects.General;
using RteLib;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.Generator
{
  internal class ModuleGenerator
  {
    private Models.CrcXml xmlCfg;
    private string pathToConfiguratioFile;
    private string pathToModuleBaseFolder;
    private string moduleLibPath;
    private GenInfoType info;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="cfgFile"> Config file which shall be generated </param>
    /// <param name="pathToCfgFile"> Path of Configuration file </param>
    /// <param name="absPathToBaseModuleFolder"> Absolute path to module base folder</param>
    public ModuleGenerator(Models.CrcXml cfgFile, string pathToCfgFile, string absPathToBaseModuleFolder)
    {
      xmlCfg = cfgFile;
      pathToConfiguratioFile = pathToCfgFile;
      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      moduleLibPath = new System.Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
    }

    /// <summary>
    /// Interface to generate the configuration
    /// </summary>
    /// <returns> Generation information </returns>
    public GenInfoType GenerateConfiguration()
    {
      // Generate Memory-Map-Header-File
      GenerateMemoryMapHeaderFile();

      // Generate C-Source-File
      GenerateConfigurationSourceFile();

      // Generate C-Header-File
      GenerateConfigurationHeaderFile();

      // Generate Rte-Interface-XML-File
      GenerateConfigurationRteInterfaceXmlFile();

      return info;
    }

    /// <summary>
    /// Interface to generate the Memory-Map-Header-File
    /// </summary>
    /// <returns> Generation information </returns>
    public void GenerateMemoryMapHeaderFile()
    {
      info.AddLogMsg(GenResources.LogMsg_StartGenMemMap);

      /* +++++ Generate Memory Mapping file +++++ */
      OsarResources.Generic.OsarGenericHelper.generateModuleMemoryMappingFile(pathToModuleBaseFolder + GenResources.GenHeaderFilePath, DefResources.ModuleName, DefResources.GeneratorName);

      info.AddLogMsg(GenResources.LogMsg_MemMapGenerated);
    }

    /// <summary>
    /// Interface to generate the configuration C-Source-File
    /// </summary>
    /// <returns> Generation information </returns>
    public void GenerateConfigurationSourceFile()
    {
      info.AddLogMsg(GenResources.LogMsg_GenSourceCfgFile);

      CSourceFile modulePBCfgSource = new CSourceFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      GeneralStartGroupObject generalStartGroup = new GeneralStartGroupObject();
      GeneralEndGroupObject generalEndGroup = new GeneralEndGroupObject();
      CFileIncludeObjects sourceIncludes = new CFileIncludeObjects();
      CFileVariableObjects sourceVariables = new CFileVariableObjects();
      CFileFunctionObjects sourceFunction = new CFileFunctionObjects();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      int actualGroupIdx;

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Prepare general data sets ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      doxygenFileHeader.AddFileBrief("Implementation of Crc module *_PBCfg.c file");
      doxygenFileHeader.AddFileName(DefResources.ModuleName + "_PBCfg.c");
      doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
      doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
      doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());
      doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
      doxygenFileGroupOpener.AddToDoxygenMasterGroup(OsarResources.Generator.Resources.genericSource.DoxygenOsarMasterGroupBSW);
      doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Prepare include strings ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      sourceIncludes.AddIncludeFile(DefResources.ModuleName + "_PBCfg.h");

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++ Prepare Source Functions ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      // Nothing to do

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Build up and generate file +++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      modulePBCfgSource.AddFileName(DefResources.ModuleName + "_PBCfg.c");
      modulePBCfgSource.AddFilePath(pathToModuleBaseFolder + GenResources.GenSourceFilePath);
      modulePBCfgSource.AddFileCommentHeader(doxygenFileHeader);
      modulePBCfgSource.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
      modulePBCfgSource.AddIncludeObject(sourceIncludes);
      modulePBCfgSource.AddGlobalVariableObject(sourceVariables);
      modulePBCfgSource.AddGlobalFunctionObject(sourceFunction);
      modulePBCfgSource.GenerateSourceFile();

      info.AddLogMsg(GenResources.LogMsg_SourceCfgFileGenerated);
    }

    /// <summary>
    /// Interface to generate the configuration C-Header-File
    /// </summary>
    /// <returns> Generation information </returns>
    public void GenerateConfigurationHeaderFile()
    {
      info.AddLogMsg(GenResources.LogMsg_GenHeaderCfgFile);

      CHeaderFile modulePBCfgHeader = new CHeaderFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      CFileDefinitionObjects headerDefinitions = new CFileDefinitionObjects();
      GeneralStartGroupObject generalStartGroup = new GeneralStartGroupObject();
      GeneralEndGroupObject generalEndGroup = new GeneralEndGroupObject();
      CFileIncludeObjects headerIncludes = new CFileIncludeObjects();
      CFileVariableObjects headerVariables = new CFileVariableObjects();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      CFileTypeObjects headerTypes = new CFileTypeObjects();
      int actualGroupIdx;


      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Prepare general data sets ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      doxygenFileHeader.AddFileBrief("Generated header file data of the Crc module.");
      doxygenFileHeader.AddFileName(DefResources.ModuleName + "_PBCfg.h");
      doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
      doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
      doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());
      doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
      doxygenFileGroupOpener.AddToDoxygenMasterGroup(OsarResources.Generator.Resources.genericSource.DoxygenOsarMasterGroupBSW);
      doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Prepare include strings ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      headerIncludes.AddIncludeFile(DefResources.ModuleName + "_Types.h");

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++++ Prepare data types +++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++ Prepare Definitions +++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* Create Det informations */
      generalStartGroup.AddGroupName("Crc Module Det Information");
      actualGroupIdx = headerDefinitions.AddCFileObjectGroup(generalStartGroup, generalEndGroup);
      headerDefinitions.AddDefinition(GenResources.GenDefineDetModuleId, 
        xmlCfg.detModuleID.ToString(), actualGroupIdx);
      headerDefinitions.AddDefinition(GenResources.GenDefineModuleUseDet, 
        xmlCfg.detModuleUsage.ToString(), actualGroupIdx);

      /* Create Crc algorithm usage informations */
      generalStartGroup.AddGroupName("Crc Algorithm Configuration");
      actualGroupIdx = headerDefinitions.AddCFileObjectGroup(generalStartGroup, generalEndGroup);
      headerDefinitions.AddDefinition(GenResources.GenDefineSoftCalcCrc32_0x04C11DB7, 
        xmlCfg.crcUseSoftCalcCrc32_0x04C11DB7.ToString(), actualGroupIdx);


      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++ Prepare Header Functions ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Build up and generate file +++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      modulePBCfgHeader.AddFileName(DefResources.ModuleName + "_PBCfg.h");
      modulePBCfgHeader.AddFilePath(pathToModuleBaseFolder + GenResources.GenHeaderFilePath);
      modulePBCfgHeader.AddFileCommentHeader(doxygenFileHeader);
      modulePBCfgHeader.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
      modulePBCfgHeader.AddDefinitionObject(headerDefinitions);
      modulePBCfgHeader.AddIncludeObject(headerIncludes);
      modulePBCfgHeader.AddGlobalVariableObject(headerVariables);
      modulePBCfgHeader.AddTypesObject(headerTypes);
      modulePBCfgHeader.GenerateSourceFile();

      info.AddLogMsg(GenResources.LogMsg_HeaderCfgFileGenerated);
    }

    /// <summary>
    /// Interface to generate the configuration Rte-Interface-XML-File
    /// </summary>
    /// <returns> Generation information </returns>
    public void GenerateConfigurationRteInterfaceXmlFile()
    {
      info.AddLogMsg(GenResources.LogMsg_GenRteMibCfgFile);
      info.AddLogMsg("Using RteLib version: v." + RteLib.VersionClass.major.ToString() + "." +
        RteLib.VersionClass.minor.ToString() + "." + RteLib.VersionClass.patch.ToString());

      // Create main instance of internal behavior file
      RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior internalBehavior = new RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior();
      RteLib.RteInterface.RteGeneralInterfaceInfo runabbleInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      RteLib.RteModuleInternalBehavior.RteRunnable initRunnable = new RteLib.RteModuleInternalBehavior.RteRunnable();
      RteLib.RteModuleInternalBehavior.RteRunnable rteRunnable = new RteLib.RteModuleInternalBehavior.RteRunnable();
      RteLib.RteModuleInternalBehavior.RteCyclicRunnable mainfunctionRunnable = new RteLib.RteModuleInternalBehavior.RteCyclicRunnable();

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++ Create Types ++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++ Create Interfaces +++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++ Create Init Runnable ++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      runabbleInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      runabbleInfo.InterfaceName = GenResources.RteMib_InitRunnableName;
      runabbleInfo.UUID = GenResources.RteMib_InitRunnableName_UUID;

      initRunnable.RunnableInfo = runabbleInfo;

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Create Cyclic Runnable +++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      runabbleInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      runabbleInfo.InterfaceName = GenResources.RteMib_MainfunctionRunnableName;
      runabbleInfo.UUID = GenResources.RteMib_MainfunctionRunnableName_UUID;

      rteRunnable = new RteLib.RteModuleInternalBehavior.RteRunnable();
      rteRunnable.RunnableInfo = runabbleInfo;

      mainfunctionRunnable = new RteLib.RteModuleInternalBehavior.RteCyclicRunnable();
      mainfunctionRunnable.Runnable = rteRunnable;
      mainfunctionRunnable.CycleTime = Convert.ToUInt16(GenResources.RteMib_MainfunctionRunnableCycleTime);

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Build up and generate file +++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      internalBehavior.ModuleName = DefResources.ModuleName;
      internalBehavior.ModuleType = RteLib.RteConfig.RteConfigModuleTypes.BSW;
      internalBehavior.UUID = GenResources.RteMib_Module_UUID;
      internalBehavior.InitRunnables.Add(initRunnable);
      internalBehavior.CyclicRunnables.Add(mainfunctionRunnable);

      internalBehavior.SaveActiveRteModuleInternalBehaviorToXml(pathToModuleBaseFolder + GenResources.GenGeneratorFilePath + "\\" + GenResources.RteCfgFileName);

      info.AddLogMsg(GenResources.LogMsg_RteMibCfgFileGenerated);
    }
  }
}

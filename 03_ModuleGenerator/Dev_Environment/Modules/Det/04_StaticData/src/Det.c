/****************************************************************************************************************************
* @file        Det.c                                                                                                        *
* @author      OSAR Team Reinemuth Sebastian                                                                                *
* @date        02-09-2017                                                                                                   *
* @brief       Implementation for Det Module Functionality                                                                  *
* @details     The Det module implements an development error trace. This file woult implement the basic functionality.     *
* @version     1.0.0                                                                                                        *
* @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
*              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
*              License, or (at your option) any later version.                                                              *
*                                                                                                                           *
*              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
*              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
*              License for more details http://www.gnu.org/licenses/.                                                       *
* last checkin :                                                                                                            *
* $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $                       *
****************************************************************************************************************************/
/** @addtogroup  Det Internal Functions
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>              Start of private include area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Det.h"
#include "Det_Cfg.h"

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>               End of private include area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
  * @struct   detUserDeviations_t
  * @brief    Struct definitions of the neccessary informations to deviate an det error
  */
typedef struct
{
  uint16 ModuleId;
  uint16 ErrorId;
  boolean used;
}detUserDeviations_t;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#define DET_START_SEC_NOINIT_VAR
#include "Det_MemMap.h"

boolean bswDet_ReleaseDet;     //!< Master variable to release the det error endless while loop

detUserDeviations_t detUserDeviations[DET_MAX_DET_USER_DEVIATIONS]; //!< Buffer for user det deviations


#define DET_STOP_SEC_NOINIT_VAR
#include "Det_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         Start of private function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#define DET_START_SEC_CODE
#include "Det_MemMap.h"

/**
  * @fnmerge	  Std_ReturnType Det_Init(void);
  * @brief      Initialize the Det Module
  * @param      none
  * @retval     Std_ReturnType would always return E_OK
  */
Std_ReturnType Det_Init(void)
{
  return E_OK;
}

/**
  * @fnmerge	  Std_ReturnType Det_InitMemory(void);
  * @brief      Initialize the Det Module
  * @param      none
  * @retval     Std_ReturnType would always return E_OK
  */
Std_ReturnType Det_InitMemory(void)
{
  /* Perform memory initialization */
  bswDet_ReleaseDet = FALSE;

  for (uint16 idx = 0; idx < DET_MAX_DET_USER_DEVIATIONS; idx++)
  {
    detUserDeviations[idx].used = FALSE;
  }

  return E_OK;
}

/**
  * @fnmerge	  Std_ReturnType Det_ReportError(uint16 moduleId, uint16 errorId);
  * @brief      API to report an det error
  * @param      uint16 Module Id >> Shall be unique within the system
  * @param      uint16 Error Id >> Shall be within an module unique
  * @retval     Std_ReturnType would always return E_NOT_OK
  * @note       If the user dose not release the Det error it would never return
  */
Std_ReturnType Det_ReportError(uint16 moduleId, uint16 errorId)
{
#ifdef DET_MODULE_ENABLED
  //TODO Impelement Search algorithm to deactivate an specific det error

  for (uint16 idx = 0; idx < DET_MAX_DET_USER_DEVIATIONS; idx++)
  {
    /* If the element is unused then break the for loop >> No deviations any more available */
    if (FALSE == detUserDeviations[idx].used)
    {
      break;
    }

    /* Check if deviation is equal to the current Det */
    if ((moduleId == detUserDeviations[idx].ModuleId) && (errorId == detUserDeviations[idx].ErrorId))
    {
      /* Deviation found >> Release Det */
      bswDet_ReleaseDet = TRUE;
      break;
    }
  }

  /* Det endless loop */
  while (FALSE == bswDet_ReleaseDet)
  {
  }

  /* Reset release det variable for next Det error */
  bswDet_ReleaseDet = FALSE;

#endif
  return E_NOT_OK;
}

/**
  * @fnmerge	  Std_ReturnType Det_AddUserDeviation(uint16 moduleId, uint16 errorId);
  * @brief      API to add an user deviation
  * @param      uint16 Module Id >> Shall be unique within the system
  * @param      uint16 Error Id >> Shall be within an module unique
  * @retval     Std_ReturnType would return E_NOT_Ok if no more user deviations are available
  * @note       This function would report itself an det error
  */
Std_ReturnType Det_AddUserDeviation(uint16 moduleId, uint16 errorId)
{
  /* Search for free deviation place in variable */
  for (uint16 idx = 0; idx < DET_MAX_DET_USER_DEVIATIONS; idx++)
  {
    if (FALSE == detUserDeviations[idx].used)
    {
      /* Found free place >> Add user deviation */
      detUserDeviations[idx].ModuleId = moduleId;
      detUserDeviations[idx].ErrorId = errorId;
      detUserDeviations[idx].used = TRUE;
      return E_OK;
    }
  }

  /* No Space any more free >> Report an Det Error */
#if (DET_MODULE_USE_DET == STD_ON)
  Det_ReportError(DET_MODULE_ID, DET_E_DET_DEVIATIONS_FULL);
#endif

  return E_NOT_OK;
}

/**
  * @fnmerge	  Std_ReturnType Det_RemoveUserDeviation(uint16 moduleId, uint16 errorId);
  * @brief      API to remove an user deviation
  * @param      uint16 Module Id >> Shall be unique within the system
  * @param      uint16 Error Id >> Shall be within an module unique
  * @retval     Std_ReturnType would return E_NOT_OK if user deviation could not be found
  */
Std_ReturnType Det_RemoveUserDeviation(uint16 moduleId, uint16 errorId)
{
  sint32 idx, idx2;
  Std_ReturnType retVal = E_NOT_OK;
  /* Seach user deviation */
  for (idx = 0; idx < DET_MAX_DET_USER_DEVIATIONS; idx++)
  {
    /* If Moulde has been found */
    if ((TRUE == detUserDeviations[idx].used) && (moduleId == detUserDeviations[idx].ModuleId) && (moduleId == detUserDeviations[idx].ModuleId))
    {
      /* Search for last user deviation in list */
      for (idx2 = DET_MAX_DET_USER_DEVIATIONS - 1; idx2 >= 0; idx2--)
      {
        if ((TRUE == detUserDeviations[idx2].used))
        {
          detUserDeviations[idx].used = detUserDeviations[idx2].used;
          detUserDeviations[idx].ModuleId = detUserDeviations[idx2].ModuleId;
          detUserDeviations[idx].ErrorId = detUserDeviations[idx2].ErrorId;

          /* Deactivate Last Deviation */
          detUserDeviations[idx2].used = FALSE;
          break;
        }
      }

      retVal = E_OK;
      break;
    }
  }

  return retVal;
}

#define DET_STOP_SEC_CODE
#include "Det_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
* @}
*/
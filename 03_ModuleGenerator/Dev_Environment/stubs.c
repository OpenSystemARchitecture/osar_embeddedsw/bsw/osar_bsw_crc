/******************************************************************************
  * @file    stubs.c 
  * @author  Reinemuth Sebastian
  * @date    20-02-2018
  * @brief   stubs-Functions
  * last checkin :
  * $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $
  *****************************************************************************/

/* Private includes ----------------------------------------------------------*/
#include "stubs.h"

/* Global variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Functions -----------------------------------------------------------------*/

/**
  * @fnmerge	void initializeSystem(void)
  * @brief      Initialize the System
  * @param      none
  * @retval     none
  * @note       This function is only an template
  */
void initializeSystem(void)
{
  while(1);
}

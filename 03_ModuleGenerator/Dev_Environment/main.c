/******************************************************************************
* @file    main.c
* @author  Reinemuth Sebastian
* @date    17-02-2016
* @brief   main-Functions
* last checkin :
* $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $
*****************************************************************************/

/* Private includes ----------------------------------------------------------*/
#include "Std_Types.h"
#include "Crc.h"
/* Global variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Functions -----------------------------------------------------------------*/
int main()
{
  uint32 crcResult;
  uint8 testData[12] = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11 };
  Crc_ReturnType retVal;

  retVal = Crc32_0x04C11DB7_Compute(&testData[0], 12, &crcResult);

  while (1);
}